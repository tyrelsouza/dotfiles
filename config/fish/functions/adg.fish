function adg
	vf activate addgene-core                                                     
    if test -d ~/code/github/addgene/addgene-core
        cd ~/code/github/addgene/addgene-core
    end
    if test -d ~/AddgeneProjects/addgene-core
		cd ~/AddgeneProjects/addgene-core
    end
end
