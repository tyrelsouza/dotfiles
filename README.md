# How To

Clone the repository, then symlink `ln -s` each file to the dotted version in your homedirectory.

ex:

```ln -s dotfiles/zshrc ~/.zshrc```

## Script
Just run ```dotfiles/INSTALL.sh```


## Requirements:
- bin/sshc
    cpan Term::Title
	cpan Perl::Critic
	cpan XML::Dumper
	cpan Curses
	cpan Curses::UI

